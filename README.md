# Density Functional Theory
This repository contains all resources related to density functional theory including
book links, slides, input files, and exercises.

## Tutorials
* [Tutorials page](https://gitlab.com/computational_physics/DFT/tree/master/tutorials/tutorial_1)

## Slides
* [Introduction to DFT](https://gitlab.com/computational_physics/DFT/blob/master/slides/introduction_to_dft%20.pptx)
* [Software tools for DFT](https://gitlab.com/computational_physics/DFT/blob/master/slides/software_for_esc.pptx)
* [Linux command line](https://gitlab.com/computational_physics/DFT/blob/master/slides/linux_command_line.pptx)

## Books
* [A Practical Introduction to DFT](https://www.amazon.com/Density-Functional-Theory-Practical-Introduction-ebook/dp/B005PS4Z3A)
* [Electronic Structure (this is a classic)](https://www.amazon.com/Electronic-Structure-Practical-Methods-Functional-ebook/dp/B00AKE1QXG/ref=pd_sim_351_3?_encoding=UTF8&pd_rd_i=B00AKE1QXG&pd_rd_r=7314cceb-9c80-11e8-800d-291f7dc96a5e&pd_rd_w=wiZrt&pd_rd_wg=nzUtm&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a180fdfb-b54e-4904-85ba-d852197d6c09&pf_rd_r=P6GF9JH9QS8MQS0529KZ&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=P6GF9JH9QS8MQS0529KZ)
* [Interacting Electrons (beyond DFT)](https://www.amazon.com/Interacting-Electrons-Theory-Computational-Approaches-ebook/dp/B01GG093E6/ref=pd_sim_351_10?_encoding=UTF8&pd_rd_i=B01GG093E6&pd_rd_r=82903618-a4a8-11e8-8ca5-3701795e9eab&pd_rd_w=tlq2r&pd_rd_wg=h5r9R&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a180fdfb-b54e-4904-85ba-d852197d6c09&pf_rd_r=Q5TJCMGNTY8NMRQ48VPX&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=Q5TJCMGNTY8NMRQ48VPX)
* [Materials Modeling (Created for undergraduates but haven't purchased the book)](https://www.amazon.com/Materials-Modelling-Density-Functional-Theory-ebook/dp/B00KBWATI0/ref=pd_sim_351_1?_encoding=UTF8&pd_rd_i=B00KBWATI0&pd_rd_r=809d1843-a4a8-11e8-91f5-8f109ce70124&pd_rd_w=099hJ&pd_rd_wg=40JzK&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a180fdfb-b54e-4904-85ba-d852197d6c09&pf_rd_r=QSMDSY5CGXVSD3NZPWY6&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=QSMDSY5CGXVSD3NZPWY6)
* [Time dependent DFT](https://www.amazon.com/Time-Dependent-Density-Functional-Theory-Concepts-Applications-ebook/dp/B00BBEXT84/ref=pd_sim_351_8?_encoding=UTF8&pd_rd_i=B00BBEXT84&pd_rd_r=82903618-a4a8-11e8-8ca5-3701795e9eab&pd_rd_w=tlq2r&pd_rd_wg=h5r9R&pf_rd_i=desktop-dp-sims&pf_rd_m=ATVPDKIKX0DER&pf_rd_p=a180fdfb-b54e-4904-85ba-d852197d6c09&pf_rd_r=Q5TJCMGNTY8NMRQ48VPX&pf_rd_s=desktop-dp-sims&pf_rd_t=40701&psc=1&refRID=Q5TJCMGNTY8NMRQ48VPX)

## Links
* [Quantum Espresso](https://www.quantum-espresso.org/)
* [XCrySDen](http://www.xcrysden.org/)